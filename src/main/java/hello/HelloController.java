package hello;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDateTime;

@Controller
@RequestMapping("/")
public class HelloController {

    @GetMapping()
    public ModelAndView index() {
        ModelAndView model = new ModelAndView();
        model.addObject("time", LocalDateTime.now());
        model.addObject("message", "test");
        model.setViewName("index");
        return model;
    }

}
